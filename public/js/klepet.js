var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

//sprememba ukaza pridruzitev(naloga 2.7):
Klepet.prototype.spremeniKanalZGeslom = function(kanal, geslo){
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });  
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  
  //nov ukaz 'zasebno' (naloga 2.6):
  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      besede = besede.join(' ');
      var kanal, geslo;
      if(besede.split('\"').length < 4){//brez gesla
        kanal = besede;
        this.spremeniKanal(kanal);
      }else{//z geslom
        besede = besede.split('\"');
        kanal = besede[1];
        geslo = besede[3];
        this.spremeniKanalZGeslom(kanal, geslo);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      besede = besede.join(' ');
      besede = besede.split('\"');
      if(besede[3]!=null){
        var tekst = {uporabnik: besede[1], sporocilo: besede[3]};
        //izpis na posiljatelejevem zaslonu:
        $('#sporocila').append("(zasebno sporocilo za "+tekst.uporabnik+"): "+tekst.sporocilo);
        this.socket.emit('sporociloZasebno', tekst);
      }else{
        sporocilo = 'Neznan ukaz.';
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};