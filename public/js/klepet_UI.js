function divElementEnostavniTekst(sporocilo) {
  return vstaviSmeskota($('<div style="font-weight: bold"></div>'), sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
       $('#kanal').text(rezultat.vzdevek +" @ "+ rezultat.kanal); //ob spremebi vzdevka popravimo vzdevek tudi zgoraj
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek +" @ "+ rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>');
    $('#sporocila').append(vstaviSmeskota(novElement, sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  //dodajanje seznama uporabnikov:
  socket.on('seznamUporabnikov', function(rezultat) {
    $('#seznam-uporabnikov').empty();
    for(var i=0; i<rezultat.seznamUporabnikov.length; i++ ){
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(rezultat.seznamUporabnikov[i]));
    }
  });

  setInterval(function() {
    socket.emit('kanali');
    //posodabljamo se seznam uporabnikov:
    socket.emit('seznamUporabnikov');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

//funkcija za vstaljanje smeskov:
function vstaviSmeskota(znak, sporocilo){
  for(var i=0;i<sporocilo.length;i++){//sprehod cez tekst
    //smajli ";)":
    if(sporocilo.indexOf(";)",i)==i){
      i=i+1;
      znak.append("<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png></img>");
    }
    //smajli ":)":
    else if(sporocilo.indexOf(":)",i)==i){
      i=i+1;
      znak.append("<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png></img>");
    }
    //smajli ":*":
    else if(sporocilo.indexOf(":*",i)==i){
      i=i+1;
      znak.append("<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png></img>");
    }
    //smajli ":(":
    else if(sporocilo.indexOf(":(",i)==i){
      i=i+1;
      znak.append("<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png></img>");
    }
    //znak "(y)":
    else if(sporocilo.indexOf("(y)",i)==i){
      i=i+2;
      znak.append("<img src=https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png></img>");
    }
    //normalna crka:
    else{
      znak.append(sporocilo.charAt(i));
    }
  }
  return znak;
}
